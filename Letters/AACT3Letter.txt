<10>
From: Edna Peabody

Dear Stranger, 

	What a splendid job you are doing! The orphanage is now equipped to cater to the 
needs of all the children. Alice has been very kind to take them all in during the 
transition, and I'm thinking she may not have been so generous without your direction. 
You've made a tremendous difference in our fair city, and I thank you!

Have a Blessed Day,
~Edna P.