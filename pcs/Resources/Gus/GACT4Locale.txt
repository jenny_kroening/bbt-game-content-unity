{
	"__type": "ChaoticMoon.Core.LocalizationContext, Assembly-CSharp",
	
	"m_localization_db": 
		{
			"c8023d62-02e9-4e9f-8d08-23c422a736bb": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Gus, I gotta tell you, I'm staring to think you'd make a fantastic V.P. of Buildin' Things."
						}
				},
			"b36a0871-9221-44ef-9b34-171d95ea6ce5": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I'm flattered, but I don't have enough time to honor my current commitments. If I ever get some help, maybe I'll consider it."
						}
				},
			"0f3e939a-7088-4715-a5e7-e96e571f679c": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "That's a great idea, you should hire some folks."
						}
				},
			"f2798cda-e340-449b-bab8-1943b722be0c": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I agree, your business is exploding. What's stopping you from hiring folks on to help you?"
						}
				},
			"9b4f1658-18f7-4958-a3e2-8e86f3451b7d": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "You can't handle this workload alone, and it's not going to stop. You have to hire people."
						}
				},
			"925c26d2-d352-4901-b9d7-0939244bab80": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I do things my way. It works fine for me, but it'd be disrespectful to push my buildin' ways on others."
						}
				},
			"cd8b6837-c04d-4d33-8388-2c6763e1bcc8": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I ain't keen on lettin' anyone down, and my projects might go sideways on someone else's watch."
						}
				},
			"005321ec-1f16-4e1a-a5f0-2c0169f6b861": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "You're a talented guy! I'm sure there are many people that would be honored to work up to your A+ standards!"
						}
				},
			"7ee27b99-a93b-4150-a464-dc813dab1529": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "That's a humble stance, but keep in mind hiring is about trust. Do you know anyone you'd be confident to hire?"
						}
				},
			"546ef7da-cc7a-4858-b291-4027177bde57": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "That may happen at first, but you'll never get ahead of demand if you don't. It's time you focused on growing your business."
						}
				},
			"0f8a1ea8-f988-42c1-b2ad-54995ef6fba3": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I'm not so sure about that. But you're right that I could really use some help."
						}
				},
			"758183f3-fbfe-410a-bee2-eea0bc59e6a4": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Come to think of it... I do know a few talented folks that could use the work. This might be a really good idea."
						}
				},
			"98992bf0-aa6d-4d32-ac84-9d858f34b10c": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "My word's my bond. I guess if someone respected that, I'd be open to bringing 'em on for a spell."
						}
				},
			"2541465a-6b53-4b02-b0e7-e79fb2fe51dc": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Hey there!"
						}
				},
			"59ab27af-7bcf-408b-9c37-a20d7d34ff56": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Wow! This place cleaned up nice and so did you!"
						}
				},
			"c2f92e91-370e-4c05-b45a-fa1337b4b9af": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Thank you kindly.  Business is good, and I'm feelin' pretty chipper.  You ready?  We got a full day ahead of us."
						}
				},
			"6c491a22-7131-45de-91b3-1ec9671ebbc3": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "I'm ready!.. I was thinking we could do another screen montage."
						}
				},
			"11867733-765c-4672-aa5e-d30f088ef620": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Sounds about right.  Just get on with it."
						}
				},
			"7bf006c2-bd4a-4d99-972b-a66ca1f4a492": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Gus, I gotta tell you, I'm starting to think you'd make a fantastic V.P. of Buildin' Things."
						}
				},
			"556322be-cda7-4185-a02b-a71b7f65c995": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "\u003Cen-US>"
						}
				},
			"26cef72b-be49-4dc8-a3c5-dc2454e3f0d0": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "\u003Cen-US>"
						}
				}
		}
}