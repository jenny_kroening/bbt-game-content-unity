{
	"__type": "ChaoticMoon.Core.LocalizationContext, Assembly-CSharp",
	
	"m_localization_db": 
		{
			"f71a7dc8-7732-4ba9-9386-ac974539f109": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Thank you for everything you've done for me!"
						}
				},
			"903ec42e-bfbc-44eb-a475-cf7677c9b50b": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "The pleasure is mine, Gus!  I've learned how our underlying beliefs are what guide our behaviors, and lead to results.  And now your results are great!"
						}
				},
			"2c1da4e8-dc2b-40ce-895e-5ec54afd229f": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Congratulations!  From this day forth, you are hereby the Victorious Person of Buildin' Stuff!"
						}
				},
			"0d5e1769-f763-4ed1-95f6-19f86db05ee7": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Oh dear, I'm afraid I'm double-booked.  Let's get a club soda soon.  Congrats again my friend!"
						}
				}
		}
}