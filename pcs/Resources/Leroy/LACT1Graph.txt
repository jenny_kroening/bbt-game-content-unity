{
	"__type": "ChaoticMoon.BBT.BBTDecisionGraph, Assembly-CSharp",
	
	"m_auto_start": false,
	
	"m_next_node_id": 35,
	
	"m_decision_nodes": 
		[
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LIntro",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 238,
						
						"y": 197
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 118,
						
						"y": 188
					},
				
				"m_start_node": false,
				
				"m_id": 1,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 2
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 3
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 4
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "3b09396d-b7fd-4bd3-b5a3-d2250f1d978f",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 362,
						
						"y": 65
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 281,
						
						"y": 56
					},
				
				"m_start_node": false,
				
				"m_id": 2,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 5
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "5bf9b1fc-a0f8-42ff-b9a8-5a987403ab46",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 363,
						
						"y": 131.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 281,
						
						"y": 122
					},
				
				"m_start_node": false,
				
				"m_id": 3,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 5
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "07fa6d56-ea02-4f95-ab51-7130ab52fc3d",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 360,
						
						"y": 203
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 279,
						
						"y": 194
					},
				
				"m_start_node": false,
				
				"m_id": 4,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 5
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "c352667b-e212-41e7-b485-5c57480a55f7",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LR1ALL",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 581.7849,
						
						"y": 133.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 460.7849,
						
						"y": 124
					},
				
				"m_start_node": false,
				
				"m_id": 5,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 6
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 7
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 8
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "ef0d0e60-e778-45ad-a79c-0a7920670112",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 730.693359,
						
						"y": 70
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 649.693359,
						
						"y": 61
					},
				
				"m_start_node": false,
				
				"m_id": 6,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 9
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "60d08219-e337-43be-8578-786a629df2ee",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 730.908447,
						
						"y": 138.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 648.908447,
						
						"y": 129
					},
				
				"m_start_node": false,
				
				"m_id": 7,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 9
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "2897fe5a-a360-4fe0-903f-21a9a86620a6",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 728.908447,
						
						"y": 219
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 647.908447,
						
						"y": 210
					},
				
				"m_start_node": false,
				
				"m_id": 8,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 10
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "fd9605ec-9d31-49c1-9aba-df70859051d9",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LRPC2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 897.4524,
						
						"y": 104.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 789.4524,
						
						"y": 95
					},
				
				"m_start_node": false,
				
				"m_id": 9,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 11
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 12
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "beea1751-6792-4b45-9f45-736a48276997",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LRS2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 892.1719,
						
						"y": 218
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 798.1719,
						
						"y": 209
					},
				
				"m_start_node": false,
				
				"m_id": 10,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 13
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 11
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 12
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "cccfe254-618d-4e11-81cc-5cc5b4df2079",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1052.52368,
						
						"y": 89
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 971.5237,
						
						"y": 80
					},
				
				"m_start_node": false,
				
				"m_id": 11,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 15
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "268506ea-7acc-4173-bbea-4dd7543bbab6",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1053.32483,
						
						"y": 153.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 971.3248,
						
						"y": 144
					},
				
				"m_start_node": false,
				
				"m_id": 12,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 16
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "53e35d9d-fa29-4131-b64a-5af875a7b088",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1049.60559,
						
						"y": 209
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 968.6056,
						
						"y": 200
					},
				
				"m_start_node": false,
				
				"m_id": 13,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 14
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "3671bbc8-6b05-4b0c-adcc-cf99ef9b0588",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "LSEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1635.79236,
						
						"y": 316
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1528.79236,
						
						"y": 307
					},
				
				"m_start_node": false,
				
				"m_id": 14,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "c0e4dd24-bac5-41bd-9392-eb4dcca37625",
				
				"m_node_type": "decision_node",
				
				"m_quest": "LACT1",
				
				"m_point_award": 1,
				
				"m_quest_award_names": 
					[
						"LACT2"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LRP3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1249.93176,
						
						"y": 99
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1155.93176,
						
						"y": 90
					},
				
				"m_start_node": false,
				
				"m_id": 15,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 17
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 19
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "ef87f125-0821-4060-8fa2-461188bef7e7",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "LRC3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1248.93176,
						
						"y": 162.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1153.93176,
						
						"y": 153
					},
				
				"m_start_node": false,
				
				"m_id": 16,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 17
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 18
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "7a338ab5-4df1-46d1-90db-0ccf0755ab87",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1434.42761,
						
						"y": 97
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1353.42761,
						
						"y": 88
					},
				
				"m_start_node": false,
				
				"m_id": 17,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 21
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "6176a140-ddfa-4cc1-b311-22db73be13cc",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1437.372,
						
						"y": 167.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1355.372,
						
						"y": 158
					},
				
				"m_start_node": false,
				
				"m_id": 18,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 20
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "67eec57c-06e4-4ccb-b112-64725f78c9d5",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1438.62,
						
						"y": 238
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1357.62,
						
						"y": 229
					},
				
				"m_start_node": false,
				
				"m_id": 19,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 14
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "1f798bbd-b19b-4ce6-b1a8-5246e707b48e",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "LCEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1639.16736,
						
						"y": 188.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1531.16736,
						
						"y": 179
					},
				
				"m_start_node": false,
				
				"m_id": 20,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "0925e8a2-5d7a-44bc-973a-b0beff645979",
				
				"m_node_type": "decision_node",
				
				"m_quest": "LACT1",
				
				"m_point_award": 2,
				
				"m_quest_award_names": 
					[
						"LACT2"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "LPEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1633.57947,
						
						"y": 101
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1526.57947,
						
						"y": 92
					},
				
				"m_start_node": false,
				
				"m_id": 21,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "5cac4297-210c-49c1-a7fc-9c915c8bda2b",
				
				"m_node_type": "decision_node",
				
				"m_quest": "LACT1",
				
				"m_point_award": 0,
				
				"m_quest_award_names": 
					[
						"LACT2"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTSceneNode, Assembly-CSharp",
				
				"m_editor_name": "Scene1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 140,
						
						"y": 331
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 20,
						
						"y": 322
					},
				
				"m_start_node": false,
				
				"m_id": 23,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 24
						}
					],
				
				"m_speaker_name_localized_key": "",
				
				"m_text_localized_key": "",
				
				"m_node_type": "decision_node",
				
				"m_quest": "LACT1",
				
				"m_zone": "Zone1",
				
				"m_scene": null,
				
				"m_change_node": false
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "Y1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 96,
						
						"y": 396
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 28,
						
						"y": 387
					},
				
				"m_start_node": false,
				
				"m_id": 24,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 25
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "5947ea22-1903-4c21-8468-1c7adf482657",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "Y2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 99,
						
						"y": 446
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 31,
						
						"y": 437
					},
				
				"m_start_node": false,
				
				"m_id": 25,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 26
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "12dfc89f-8cd2-42d7-a762-bef74a78e973",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "L1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 101,
						
						"y": 501
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 33,
						
						"y": 492
					},
				
				"m_start_node": false,
				
				"m_id": 26,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 27
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "74565c20-83dd-4565-98cb-fe4360719c6f",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "C1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 239,
						
						"y": 337.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 170,
						
						"y": 328
					},
				
				"m_start_node": false,
				
				"m_id": 27,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 30
						}
					],
				
				"m_speaker_name_localized_key": "366090e1-7ab3-4001-b33a-88ac01cfd62d",
				
				"m_text_localized_key": "a0d378fc-2bc4-4114-9a24-c5094cd814b8",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTMiniGameNode, Assembly-CSharp",
				
				"m_editor_name": "MG_LACT1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 155,
						
						"y": 262
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 7,
						
						"y": 252
					},
				
				"m_start_node": false,
				
				"m_id": 29,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 23
						}
					],
				
				"m_speaker_name_localized_key": "",
				
				"m_text_localized_key": "",
				
				"m_node_type": "decision_node",
				
				"m_award_points": false,
				
				"m_mini_game_prefab_name": "MiniGames/Z1_Leroy/MG_LACT1",
				
				"m_quest": "LACT1",
				
				"m_pcs_ui_root": null,
				
				"m_mini_game_prefab": null,
				
				"m_game_over": false,
				
				"m_complete": false,
				
				"m_replayed": false,
				
				"m_load_mini_game": false,
				
				"m_quest_award_names": null,
				
				"m_score_override": -1,
				
				"m_camera": null,
				
				"m_graph": null
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "Y3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 237,
						
						"y": 394
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 169,
						
						"y": 385
					},
				
				"m_start_node": false,
				
				"m_id": 30,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 1
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "3b09396d-b7fd-4bd3-b5a3-d2250f1d978f",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "00",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 89,
						
						"y": 21
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 21,
						
						"y": 12
					},
				
				"m_start_node": true,
				
				"m_id": 31,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 32
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "5947ea22-1903-4c21-8468-1c7adf482657",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": false
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "01",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 93,
						
						"y": 71
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 25,
						
						"y": 62
					},
				
				"m_start_node": false,
				
				"m_id": 32,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 33
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "5947ea22-1903-4c21-8468-1c7adf482657",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": false
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "02",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 94,
						
						"y": 122
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 26,
						
						"y": 113
					},
				
				"m_start_node": false,
				
				"m_id": 33,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 34
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "3da0a994-6280-4747-aa45-18af931626fd",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": false
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "03",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 92,
						
						"y": 171
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 24,
						
						"y": 162
					},
				
				"m_start_node": false,
				
				"m_id": 34,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 29
						}
					],
				
				"m_speaker_name_localized_key": "6e8b2858-962b-4108-807d-ce4f72d3ef26",
				
				"m_text_localized_key": "3da0a994-6280-4747-aa45-18af931626fd",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			}
		],
	
	"m_start_node_index": 0,
	
	"m_current_node": null
}