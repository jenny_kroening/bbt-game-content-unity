{
	"__type": "ChaoticMoon.Core.LocalizationContext, Assembly-CSharp",
	
	"m_localization_db": 
		{
			"0d1a24ab-39cc-4d78-9d05-6e23134cae1d": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "What's shakin' bacon?"
						}
				},
			"04216568-f077-4c1c-8cf4-b93b538d7472": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "A training exercise! We'll try to get your customers to smile, but first, we need you to create better pairings of your animals and the right kind of transportation."
						}
				},
			"77541313-4a9f-422e-a2c3-b35466370061": 
				{
					"__type": "ChaoticMoon.Core.LocalizationContext+LocalizationEntry, Assembly-CSharp",
					
					"m_localized_string_db": 
						{
							"en-US": "Bim bam! I'm ready Freddy. Hit me!"
						}
				}
		}
}