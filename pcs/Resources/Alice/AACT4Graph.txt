{
	"__type": "ChaoticMoon.BBT.BBTDecisionGraph, Assembly-CSharp",
	
	"m_auto_start": false,
	
	"m_next_node_id": 28,
	
	"m_decision_nodes": 
		[
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "AIntro",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 354,
						
						"y": 258.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 233,
						
						"y": 249
					},
				
				"m_start_node": false,
				
				"m_id": 2,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 3
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 4
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 5
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "bd25258b-2742-4fd6-9701-1bcc26d3140d",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 477,
						
						"y": 141
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 396,
						
						"y": 132
					},
				
				"m_start_node": false,
				
				"m_id": 3,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 6
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "8e8a152c-3ca6-4152-ad25-cbc6f390ca47",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 491,
						
						"y": 250.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 409,
						
						"y": 241
					},
				
				"m_start_node": false,
				
				"m_id": 4,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 6
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "e07160ec-a3ee-44b2-9c82-e934d8ffbe6c",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 490,
						
						"y": 362
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 409,
						
						"y": 353
					},
				
				"m_start_node": false,
				
				"m_id": 5,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 7
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "fac9b82d-297f-4a40-b407-45c8e3fb837c",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARPC1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 649,
						
						"y": 176.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 541,
						
						"y": 167
					},
				
				"m_start_node": false,
				
				"m_id": 6,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 10
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 9
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 8
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "e235d38e-4709-4086-b797-6f987f5eaf39",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARS1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 636,
						
						"y": 322.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 541,
						
						"y": 313
					},
				
				"m_start_node": false,
				
				"m_id": 7,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 10
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 9
						},
						{
							"__type": "ChaoticMoon.BBT.BBTDecisionLink, Assembly-CSharp",
							
							"m_decision_node_id": 8,
							
							"m_localization_key": ""
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "537502d0-6698-4894-9963-446956cf5684",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 832,
						
						"y": 160
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 751,
						
						"y": 151
					},
				
				"m_start_node": false,
				
				"m_id": 8,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 11
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "cd6ce1cd-8f3a-455e-9b96-d208369a11d0",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 835,
						
						"y": 244.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 753,
						
						"y": 235
					},
				
				"m_start_node": false,
				
				"m_id": 9,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 11
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "8dc57010-d29d-44f6-b6b4-91680fe25697",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 838.127441,
						
						"y": 338
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 757.127441,
						
						"y": 329
					},
				
				"m_start_node": false,
				
				"m_id": 10,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 12
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "d9bd8453-15d0-47b0-9312-41aecc027106",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARPC2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1017.12744,
						
						"y": 196.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 909.127441,
						
						"y": 187
					},
				
				"m_start_node": false,
				
				"m_id": 11,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 13
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 14
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 15
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "5bf259d1-3358-415a-a4a0-97bd19fadf99",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARS2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1008.25488,
						
						"y": 335.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 913.2549,
						
						"y": 326
					},
				
				"m_start_node": false,
				
				"m_id": 12,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 15
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 13
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "679fafc0-3a5f-418d-aa58-2d970da22630",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1174.03223,
						
						"y": 155
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1093.03223,
						
						"y": 146
					},
				
				"m_start_node": false,
				
				"m_id": 13,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 16
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "eb7626a2-df3c-4211-9f89-6c962d684ed6",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1179.873,
						
						"y": 247.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1097.873,
						
						"y": 238
					},
				
				"m_start_node": false,
				
				"m_id": 14,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 16
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "ad56cb09-daff-4da8-b41a-a33cf42eed46",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1183.064,
						
						"y": 340
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1102.064,
						
						"y": 331
					},
				
				"m_start_node": false,
				
				"m_id": 15,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 17
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "d39b9d37-395e-4a5b-b54d-a0c4d44cfe90",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARPC3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1352.15918,
						
						"y": 196.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1244.15918,
						
						"y": 187
					},
				
				"m_start_node": false,
				
				"m_id": 16,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 19
						},
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 18
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "b66398eb-f8d3-4a29-be1d-ecc941eb4690",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "ARS3",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1346.15918,
						
						"y": 313.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1251.15918,
						
						"y": 304
					},
				
				"m_start_node": false,
				
				"m_id": 17,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 20
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "922f5d92-3b2f-4cfb-8116-506dff483595",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YP4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1508.89063,
						
						"y": 159
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1427.89063,
						
						"y": 150
					},
				
				"m_start_node": false,
				
				"m_id": 18,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 21
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "20f08bc4-b826-4358-91bb-dec2ea995122",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YC4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1515.89063,
						
						"y": 255.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1433.89063,
						
						"y": 246
					},
				
				"m_start_node": false,
				
				"m_id": 19,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 22
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "76706ceb-ab47-41d2-8bbe-3b5fdb37ff6d",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTDecisionNode, Assembly-CSharp",
				
				"m_editor_name": "YS4",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1521.44531,
						
						"y": 364
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1440.44531,
						
						"y": 355
					},
				
				"m_start_node": false,
				
				"m_id": 20,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 23
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "6b1b194e-1694-4dc1-867a-43599fea6653",
				
				"m_node_type": "decision_node"
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "APEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1734.8999,
						
						"y": 165.679565
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1626.8999,
						
						"y": 156.179565
					},
				
				"m_start_node": false,
				
				"m_id": 21,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "80060c6d-82f4-4194-89a5-7031535a3244",
				
				"m_node_type": "decision_node",
				
				"m_quest": "AACT4",
				
				"m_point_award": 0,
				
				"m_quest_award_names": 
					[
						"AACT5"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "ACEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1734.21069,
						
						"y": 256.306763
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1626.21069,
						
						"y": 246.806763
					},
				
				"m_start_node": false,
				
				"m_id": 22,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "907f4850-5fd9-4724-b4b1-abe457bc61f0",
				
				"m_node_type": "decision_node",
				
				"m_quest": "AACT4",
				
				"m_point_award": 2,
				
				"m_quest_award_names": 
					[
						"AACT5"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTAwardNode, Assembly-CSharp",
				
				"m_editor_name": "ASEnd",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1731.8999,
						
						"y": 367.679565
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 1623.8999,
						
						"y": 358.179565
					},
				
				"m_start_node": false,
				
				"m_id": 23,
				
				"m_next_node_links": 
					[],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "4bc5c657-6a6d-44c0-b20a-fecea38d997d",
				
				"m_node_type": "decision_node",
				
				"m_quest": "AACT4",
				
				"m_point_award": 1,
				
				"m_quest_award_names": 
					[
						"AACT5"
					],
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "Y1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 111,
						
						"y": 106
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 43,
						
						"y": 97
					},
				
				"m_start_node": true,
				
				"m_id": 24,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 25
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "0e69626b-e985-48f5-98a6-a192e0ee4fa5",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "A1",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 109,
						
						"y": 183.5
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 40,
						
						"y": 174
					},
				
				"m_start_node": false,
				
				"m_id": 25,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 26
						}
					],
				
				"m_speaker_name_localized_key": "594227a1-9993-4a76-bc7b-12deda130f24",
				
				"m_text_localized_key": "58ad820f-7e24-451b-9263-07e0b49462eb",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			},
			{
				"__type": "ChaoticMoon.BBT.BBTNextNode, Assembly-CSharp",
				
				"m_editor_name": "Y2",
				
				"m_editor_link_out_pos": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 113,
						
						"y": 260
					},
				
				"m_editor_position": 
					{
						"__type": "ChaoticMoon.Core.Vector2Serializer, Assembly-CSharp",
						
						"x": 45,
						
						"y": 251
					},
				
				"m_start_node": false,
				
				"m_id": 26,
				
				"m_next_node_links": 
					[
						{
							"__type": "ChaoticMoon.DecisionGraph.DecisionNodeLink, Assembly-CSharp",
							
							"m_decision_node_id": 2
						}
					],
				
				"m_speaker_name_localized_key": "71ef1c74-ae07-4707-b742-22d36aceeef6",
				
				"m_text_localized_key": "b090801e-cebb-4274-9d0b-3fae35d84682",
				
				"m_node_type": "decision_node",
				
				"m_has_dialog": true
			}
		],
	
	"m_start_node_index": 0,
	
	"m_current_node": null
}